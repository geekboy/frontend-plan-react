var path = require('path')
var webpack = require('webpack');

module.exports = { 
    entry: './src/app.js',
    output: { 
        path: __dirname,
        filename: "build/bundle.js",
        publicPath: "/build"
    },
    module: {
        loaders: [ 
            { test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel', query: { presets: ['es2015', 'react'] } }, //同时支持es6 react
            { test: /\.css$/, loader: "style!css" },
            { test: /\.scss$/, loader: "style!css!sass" }, //sass加载器
        ]
    },
    devServer: {
        historyApiFallback: true,
        noInfo: true
    },
    resolve: {
        extensions: ['', '.js', '.jsx','.json']
    }, 
    devtool: '#eval-source-map'
};