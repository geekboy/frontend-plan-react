## 总结
使用React开发完整个DEMO花费的时间大约为：9小时。
在使用React开发的过程中发现一些问题或者是缺点，我把它大概总结了一下：

缺点：
1. 表单的单向数据绑定确实没有VUE的双向数据绑定来的方便。
1. ES6、ES5、JAX，三者写法语法区别有些大了。
1. 没有方便好用的模板视图语言，只能写JS来渲染。
1. 储存在state中的数据，如果存的是对象，那么用steState来修改该对象局部非常的不方便。

优点：
1. 渲染速度快。
1. 社区支持很强大。
1. 编写的组件高度可以复用，维护成本降低。
1. 组件编写很流畅，单向数据流很容易理解，代码可读性高。

总的来说，没有vue用起来爽。

预览地址：
http://case.tiesec.org/frontend-plan/react/index.html

