import React from 'react';
import ReactDOM from 'react-dom';
import {Router,Route,IndexRoute,hashHistory} from 'react-router';

import './css/public.css';
import './css/weui.css';

import Board from './pages/board/board'; 
import Search from './pages/search/search'; 
import Comment from './pages/comment/comment'; 
import Subject from './pages/subject/subject'; 
import List from './pages/list/list';

class App extends React.Component {  

	render() {

		return(
			<div>
				{ this.props.children }
			</div> 
		)
	}
}

ReactDOM.render((
	<Router history={hashHistory}>
		<Route path="/" component={App}>
			<IndexRoute component={Board}/>
			<Route path="/Board" component={Board}/> 
			<Route path="/Search" component={Search}/> 
			<Route path="/Comment" component={Comment}/>
			<Route path="/Subject/:id" component={Subject}/>
			<Route path="/List/:type" component={List}/>
		</Route>
	</Router>
	),document.getElementById("app")
);

