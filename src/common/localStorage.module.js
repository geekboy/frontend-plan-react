class LocalStorage {
	constructor () {
		if ( localStorage.data !== "{}"  && localStorage.data === undefined ) {
			this.data = {};
		}
	}
	get data () {
		this._data = JSON.parse(localStorage.data);
		return this._data;
	}
	set data (value) { 
		this._data = value;  
		localStorage.data = JSON.stringify(value); 
	}

} 

export default{
	LocalStorage
}