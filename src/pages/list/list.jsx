import React from 'react';
import ReactDOM from 'react-dom';

import Container from '../../components/container/container'; 
import Listitem from '../../components/listitem/listitem';
import baseUrl from '../../basedir.js'; 

class LIst extends React.Component { 

	constructor() {super();

		this.state = {
			containerFull : {
				width : true,
				height : true
			},
			items :[],
			id : ''
		}
	}

	componentWillMount() {

	             $.ajax({ 
	                       url : `${baseUrl.data}${this.props.params.type.toLowerCase()}.json`,
	                       type : "GET", 
	                       data : "json",
	                       success : (data)=>{  
	                       	     this.setState({items:data.subjects});   
	                       }
	             });

	}

 	render () {

 		let nodes = this.state.items.map(function(item) {
 			return (
 				<Listitem item={item} href={'#/subject/' + item.id} />
 			)
 		});

		return (  
		            <div className ="weui-panel weui-panel_access">
			            <div className ="weui-panel__hd">in_theaters</div>
			            <div className ="weui-panel__hd full" style={{padding:'0px'}} >
			                   	{ nodes }
			            </div> 
		            </div>
		)
 	}

}

export default LIst;