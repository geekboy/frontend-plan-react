import React from 'react';
import ReactDOM from 'react-dom';

import Header from '../../components/header/header';   
import Container from '../../components/container/container';
import Tabbar from '../../components/tabbar/tabbar';
import Rlitem from '../../components/rlitem/rlitem';
import baseUrl from '../../basedir.js'; 


class ResultItem extends React.Component {

	constructor (){super(); 

		this.go=()=>{
			window.location.href = this.props.href;
		}
	}

	render() {
		return (
	 		<div className="weui-cell weui-cell_access" onClick={this.go} >
			   <div className="weui-cell__bd weui-cell_primary">
			          <p>{this.props.title}</p>
			    </div>
			</div>
		)
	}
}
 

class Search extends React.Component {

	constructor() {super();  

	    this.state = {
	    	containerFull : {
			width : true,
			height : false
		},
		style : {
			transformOrigin : "0px 0px 0px",
			opacity : "1",
			transform : "scale(1, 1)"
		},
		isSearch : false,
		searchStr : '',
		searchItems : []
	    }

	    // alias
	    this.s = this.setState;    

	    //handle
	    this.handleSerach=()=>{
	             $.ajax({ 
	                       url : `${baseUrl.data}search_data.json`,
	                       type : "GET", 
	                       data : "json",
	                       success : (data)=>{ 
	                       	     this.setState({searchItems:data.subjects});
	                       }
	             });

	    } 
	}
	render() {
		let resultNodes = this.state.searchItems.map(function(item) {
		      return (
	                       	<ResultItem key={item.id} title={item.title} href={'#/subject/'+item.id} />
		      );
		});

		return (
			<div>
				<Header title="Search" />
				<Container containerFull={this.state.containerFull} >
					<div className={'weui-search-bar ' +  ( this.state.isSearch ? 'weui-search-bar_focusing' : '') } onClick={e=>{e.s();this.s({isSearch:true})}} > 
					            <form className="weui-search-bar__form">
					                <div className="weui-search-bar__box">
					                    <i className="weui-icon-search"></i>
					                    <input className="weui-search-bar__input"  placeholder="搜索" required="" type="search"  value={this.state.searchStr} onChange={e=>{this.s({searchStr:e.target.value});this.handleSerach()}} />  
					                    <a className="weui-icon-clear" onClick={e=>{e.s();this.s({searchStr:'',searchItems:[]});}} ></a>
					                </div>
					                <label className="weui-search-bar__label"  style={this.state.style}>
					                    <i className="weui-icon-search"></i> 
					                    <span>搜索</span>
					                </label>
					            </form>
					            <a href="javascript:" className="weui-search-bar__cancel-btn" onClick={e=>{e.s();this.s({isSearch:false,searchItems:[]});}} >取消</a>
					</div>
					<div className="weui-cells searchbar-result" >
						{resultNodes}
				              </div>
				</Container>
				<Tabbar active={1} />
			</div>
		);
	}
}

export default Search;