import React from 'react';
import ReactDOM from 'react-dom';

import Header from '../../components/header/header';   
import Container from '../../components/container/container';
import Tabbar from '../../components/tabbar/tabbar'; 
import CommentBox from '../../components/commentBox/commentBox'; 

import baseUrl from '../../basedir.js'; 

class Comment extends React.Component {

	constructor() {super(); 
	    this.state = {
	    	containerFull : {
			width : true,
			height : false
		}
	    }
	}

	render() { 
		return (
			<div>
				<Header title="Comment" />
				<Container containerFull={this.state.containerFull} > 
					<CommentBox /> 
				</Container>
				<Tabbar active={2} />
			</div>
		);
	}
}

export default Comment;