import React from 'react';
import ReactDOM from 'react-dom';

import Container from '../../components/container/container';
import Header2 from '../../components/header2/header2';

import baseUrl from '../../basedir.js'; 


class Article extends React.Component { 

	render (){
		return ( 
			<article className="weui-article">
			    <section>
			        <section>
			            <p>
			                <img src={this.props.item.images.large} />
			            </p>
			        </section>
			        <h1>
			            {this.props.item.title}({this.props.item.year})
			        </h1>
			        <section>
			            <h2 className="title">
			                {this.props.item.original_title}
			            </h2>
			            <p>
			                {this.props.item.summary}
			            </p>
			        </section>
			    </section>
			</article>
		)
	}
}


class Subject extends React.Component {

	constructor() {super();

		this.state = {
			containerFull : {
				width : true,
				height : true
			},
			item : {},
			id : ''
		}
	}

	componentWillMount() {
	             $.ajax({ 
	                       url : `${baseUrl.data}subject.json?id=${this.props.params.id}`,
	                       type : "GET", 
	                       data : "json",
	                       success : (data)=>{  
	                       	     this.setState({item:data});   
	                       }
	             });
	}

	render() { 
		return (<div>
			{ ( JSON.stringify(this.state.item) === "{}" ) ? null : <Header2 title={this.state.item.title} /> }
			<Container containerFull={this.state.containerFull} >
				{ ( JSON.stringify(this.state.item) === "{}" ) ? null : <Article item={this.state.item} /> }
			</Container>
		 </div>
		)
	}
}

export default Subject; 