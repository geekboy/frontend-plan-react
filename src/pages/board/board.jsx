import React from 'react';
import ReactDOM from 'react-dom';

import Header from '../../components/header/header';   
import Container from '../../components/container/container';
import Tabbar from '../../components/tabbar/tabbar';
import Rlitem from '../../components/rlitem/rlitem';

import baseUrl from '../../basedir.js'; 

class Board extends React.Component {

	constructor() {

	    super();

	    this.state = {
	    	containerFull : {
			width : false,
			height : false
		}
	    } 
	}

	render() { 
		return (
			<div>
				<Header title="Ranking List" />
				<Container containerFull={this.state.containerFull} >
					<Rlitem lable="In theaters" imgsrc={baseUrl.img + "/hot.png"} href={'#/List/In_theaters'} />
					<Rlitem lable="Coming soon" imgsrc={baseUrl.img + "/coming.png"} href={'#/List/coming_soon'} />
					<Rlitem lable="Top 250" imgsrc={baseUrl.img + "/top.png"}  href={'#/List/top250'} />
				</Container>
				<Tabbar active={0} />
			</div>
		);
	}
}

export default Board;