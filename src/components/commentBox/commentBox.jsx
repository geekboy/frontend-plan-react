import React from 'react';
import ReactDOM from 'react-dom';
 
import common from '../../common/common.js'; 
import LocalStorage from '../../common/localStorage.module.js';

const _localStorage = new LocalStorage.LocalStorage(); 
Date.prototype.format = common.format;

class CommentLIst extends React.Component {

	render  (){
		return (
			<div className="weui-media-box weui-media-box_text">
				<h4 className="weui-media-box__title">{this.props.item.userNickname}&nbsp;Say:</h4>
				<p className="weui-media-box__desc">{this.props.item.userComContent}</p>
				<ul className="weui-media-box__info">
				    <li className="weui-media-box__info__meta"></li>
				    <li className="weui-media-box__info__meta">{this.props.item.dateTime}</li>
				    <li className="weui-media-box__info__meta weui-media-box__info__meta_extra">{this.props.item.userWebsite}</li>
				</ul>
			</div>
		)
	}
}

class CommentForm extends React.Component { 

	constructor  () {super();

		this.state = {
			comment_nickname : '',
			comment_website : '',
			comment_content : '' 
		}

		// alias
	    	this.s = this.setState;    

	    	//methods
	    	this.handleCommentSubmit = () => { 
			if (  Object.keys(_localStorage.data).length === 0  ) {   
			      let localData = { comments : [] };
			      localData.comments.push({
			        userNickname : this.state.comment_nickname,
			        userWebsite : this.state.comment_website,
			        userComContent : this.state.comment_content,
			        dateTime : (new Date).format('yyyy-MM-dd h:m:s')
			      });  
			      _localStorage.data = localData; 
			} else { 
			      let localData = _localStorage.data;
			      localData.comments.push({
			        userNickname : this.state.comment_nickname,
			        userWebsite : this.state.comment_website,
			        userComContent : this.state.comment_content,
			        dateTime : (new Date).format('yyyy-MM-dd h:m:s')
			      }); 
			      _localStorage.data = localData; 
			} 
			this.props.onCommentSubmit( _localStorage.data.comments[_localStorage.data.comments.length - 1] );
		}

	}

	render  (){
		return (
		            <div className="weui-panel">
		                <div className="weui-cells__title">
		                    I need to make a comment:
		                </div>
		                <div className="weui-cells weui-cells_form">
		                    <div className="weui-cell">
		                        <div className="weui-cell__hd">
		                            <label className="weui-label">
		                                Nickname:
		                            </label>
		                        </div> 
		                        <div className="weui-cell__bd">
		                            <input className="weui-input" defaultValue=""  value={this.state.comment_nickname}  onChange={e=>{this.s({comment_nickname:e.target.value})}} placeholder="Jack"  type="text"  />
		                        </div>
		                    </div>
		                    <div className="weui-cell">
		                        <div className="weui-cell__hd">
		                            <label className="weui-label">
		                                Website:
		                            </label>
		                        </div>
		                        <div className="weui-cell__bd">
		                            <input className="weui-input" defaultValue=""  value={this.state.comment_website}  onChange={e=>{this.s({comment_website:e.target.value})}}  placeholder="http://www.tiesec.org/"  type="text" />
		                        </div>
		                    </div>
		                    <div className="weui-cell">
		                        <div className="weui-cell__bd">
		                            <textarea className="weui-textarea"   defaultValue=""  value={this.state.comment_content}  onChange={e=>{this.s({comment_content:e.target.value})}}   placeholder="blablabla..."
		                            rows="3" >
		                            </textarea>
		                            <div className="weui-textarea-counter">
		                                <span>0</span>/200
		                            </div>
		                        </div>
		                    </div>
		                    <a href="javascript:;" className="weui-btn weui-btn_default" onClick={this.handleCommentSubmit} >
		                        SUBMIT
		                    </a>
		                </div>
		            </div>
		)
	}
}
 

class CommentBox extends React.Component { 

	constructor() { super(); 
		this.state = {
		 	comments : []
		}

		 this.handleCommentSubmit =(comment) => { 
		 	this.state.comments.push(comment);
	    		this.setState({
	    			comments :this.state.comments
	    		});
		}
	}

	componentWillMount() { 

		if (  Object.keys(_localStorage.data).length !== 0  ) { 
		      _localStorage.data.comments.forEach((data) => this.state.comments.push(data));
		}

	}

	render() {

		let nodes = this.state.comments.map(function(item,index){
			return (
				<CommentLIst  key={index} item={item} />
			);
		});

		return (
		    <div>
		            <div className="weui-panel">
		                <div className="weui-panel__hd">
		                    Recent comments list:
		                </div>
		                <div className="weui-panel__bd" >
				 {nodes}
		                </div>
		            </div>
			<CommentForm onCommentSubmit={this.handleCommentSubmit} />
		   </div>
		);
	}
}

export default CommentBox;