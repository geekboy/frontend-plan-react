import React from 'react';
import ReactDOM from 'react-dom';

import baseUrl from '../../basedir.js'; 
import "./tabbar.css";

class Tabbar extends React.Component {

 	constructor (props) {  
		super(); 
		this.state = {}; 
 	}

	render() {
		this.state.buttons =  [
    		       {
    		       	switch : this.props.active === 0 ? baseUrl.img + "rl-2.png" : baseUrl.img + "rl-1.png"
    		       },
    		       {
    		       	switch : this.props.active === 1 ? baseUrl.img + "sc-2.png" : baseUrl.img + "sc-1.png"
    		       },
    		       {
    		       	switch : this.props.active === 2 ? baseUrl.img + "pf-2.png" : baseUrl.img + "pf-1.png"
    		       }
    		]; 

		return ( 
			<div className="weui-tabbar">
			        <a className="weui-tabbar__item" href="#/board" >
		                          <img src={this.state.buttons[0].switch} className="weui-tabbar__icon" />
			             <p className="weui-tabbar__label">Ranking List</p>
			         </a>
			        <a className="weui-tabbar__item" href="#/search" >
		                          <img src={this.state.buttons[1].switch} className="weui-tabbar__icon" />
			             <p className="weui-tabbar__label">Search</p>
			         </a>
			        <a className="weui-tabbar__item" href="#/comment" >
		                          <img src={this.state.buttons[2].switch} className="weui-tabbar__icon" />
			             <p className="weui-tabbar__label">Comment</p>
			         </a>
			</div>
		);
	}
}

export default Tabbar;