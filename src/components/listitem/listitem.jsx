import React from 'react';
import ReactDOM from 'react-dom';
  
class Listitem extends React.Component { 

 	constructor (){ super();


 		this.go=()=>{
 			window.location.href = this.props.href;
 		}

 	}

 	render () {  
		return (  
			 <a className="weui-media-box weui-media-box_appmsg" onClick={this.go} >
			    <div className="weui-media-box__hd">
			        <img className="weui-media-box__thumb" src={this.props.item.images.medium}  />
			    </div>
			    <div className="weui-media-box__bd">
			        <h4 className="weui-media-box__title">{this.props.item.title}</h4>
			        <p className="weui-media-box__desc">
			                <div className="desc">{this.props.item.original_title}}({this.props.item.year})</div>
			                <div className="desc">{this.props.item.directors[0].name}</div>
			        </p>
			    </div>
			    <div className="weui-media-box__bd rating" style={{flex:'0.2'}} >
			            {this.props.item.rating.average}
			    </div>
			</a>
		)
 	}

}

export default Listitem;
