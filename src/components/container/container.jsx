import React from 'react';
import ReactDOM from 'react-dom';

import "./container.css";

class Container extends React.Component {

 	constructor (props) {  
		super(); 
		this.state = {}; 
 	}

	render() {
		this.state._className = "container";
		this.state._style = {};
		this.state._style.padding =  this.props.containerFull.width ? "0px" : "15px";
		this.state._style.height =  this.props.containerFull.height ? "calc(100vh - 40px)" : "calc(100vh - 91px)";  
		return (
		       <div className={this.state._className} style={this.state._style}>{this.props.children}</div>
		);
	}
}

export default Container;