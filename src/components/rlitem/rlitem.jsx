import React from 'react';
import ReactDOM from 'react-dom';

import "./rlitem.css";

class Rlitem extends React.Component {

 	constructor () {   super(); 

		this.go =()=>{
			window.location.href = this.props.href;
		}
 	}



	render() { 
		return (
		     	<li className="rl-item"  href="href" onClick={this.go}>  
			         <p>{this.props.lable}</p>
			         <img src={this.props.imgsrc} />
			</li>
		);
	}

}
export default Rlitem; 
