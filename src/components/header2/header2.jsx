import React from 'react';
import ReactDOM from 'react-dom';

import baseUrl from '../../basedir.js'; 
import "./header2.css";

class Header2 extends React.Component {

	constructor () {super();

		this.back=()=>{
			window.history.go(-1);
		}
	}

	render() {
		return (
		          	<div className="header2" >
				<div className="nav-return-button"  onClick={this.back} > 
					<img  src={baseUrl.img+'return.png'} /><span>&nbsp;Return</span>
			             </div>
				<div className="nav-title-block" > { this.props.title } </div>
				<div className="nav-return-button nav-block">&lt; Return</div>
			</div>
		);
	}
}

export default Header2;





